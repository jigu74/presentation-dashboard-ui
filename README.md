This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app), using the [Redux](https://redux.js.org/) and [Redux Toolkit](https://redux-toolkit.js.org/) template.

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.


## Project description

This project was created using create-react-app with redux toolkit as template.
Project was devloped using
`node --version`
v14.16.0
`npm --v`
6.14.11

Third party libraries besides React -- create-react-app 
- Redux
- Redux toolkit
- Material UI
- highcharts.js
- highcharts-react-official
- react-select
- react-router-dom
- npx

Things todo - not done for demo.
Unit tests using React-test library
E2E tests
