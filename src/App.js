import React from 'react';
import './App.css';
import DashboardContainer from './Containers/Dashboards/DashboardContainer';
import Grid from '@material-ui/core/Grid';

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

function App() {
  return (
    <div class='App'>
      <Router>
        <Grid item xs={12}>
          <div class='navbar'>
            <ul>
              <li>Mary</li>
            </ul>
          </div>
        </Grid>
        <Grid item xs={12}>
          <div class='header'>
            Prealize Health
          </div>
        </Grid>
         <Grid item xs={2}>
          <div class='sidebar'>
            <nav>
              <ul>
                <li>
                  <Link to="/dashboard/1">Default dashboard</Link>
                </li>
                <li>
                  <Link to="/dashboard/2">Template Dashboard(Not Working:Demo Only)</Link>
                </li>
                <li>
                  <Link to="/dashboard/3">Saved Dashboard(Not Working:Demo Only)</Link>
                </li>
              </ul>
            </nav>
          </div>
         </Grid>
         <Grid item xs={10}>
          <div class='mainContent flexContainer'>
            <div>
              <Switch>
              <Route path="/">
                <DashboardContainer/>
              </Route>
              <Route path="/dashboard/:id">
                <DashboardContainer/>
              </Route>
            </Switch>
            
            </div>
            
          </div>
         </Grid>
      </Router>
  </div>
  );
}
 
export default App;
