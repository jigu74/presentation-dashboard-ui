import { createSlice } from '@reduxjs/toolkit';
import {groupBy} from '../../utils/prepareDataSet';
import {records} from '../../data/records';
import { populateAgeGroups} from '../../utils/prepareDataSet';
const recordsWithAge = populateAgeGroups(records);
// Filters should be persited on DB for Presaved Dashboard
const initialState = {
  records: recordsWithAge,
  filters:{
      gender: {
          selected:['female', 'male', 'other']
      },// options and selected
      zipcode: {},
      employment_status: {},
      education: {},
      marital_status: {
          selected:['married', 'single', 'other']
      },
      ancestry: {},
      military_service: {
          selected:['yes', 'no']
      },
      disease: {
          selected:[]
      },
      ageGroupId: {
      }
  }
};

export const dashboardSlice = createSlice({
  name: 'dashboard',
  initialState,
  // The `reducers` field lets us define reducers and generate associated actions
  reducers: {
    initialLoad: (state, action) => {
        Object.keys(state.filters).forEach((filterName) =>{
            let filterOptions = groupBy(state.records, filterName).map((g) =>({
                value:g.key,
                label: g.labelWithSummary
            }));
            state.filters[filterName].options=filterOptions;
        });
        // state.filters = action.payload;
        state.records = filterDataSet(state);
    },
    // Use the PayloadAction type to declare the contents of `action.payload`
    filterRecords: (state, action) => {
        const {
            name,
            valArr
        } = action.payload;

        state.filters[name].selected =  valArr.map(val => val.value);
        state.records = filterDataSet(state);
    },
  }
});

const filterDataSet = (state)=>{
    const filterKeys = Object.keys(state.filters).filter((key) => {
        return state.filters[key].selected?.length>0;
    });
    return recordsWithAge.filter((record) =>{
        return filterKeys.every((key) =>{
            return state.filters[key].selected.includes(record[key])
        });
    })
    //return [];
     
}

export const { initialLoad, filterRecords } = dashboardSlice.actions;

export const selectRecords = (state) => state.dashboard.records;
export const selectFilters = (state) => state.dashboard.filters;
export const selectPrimaryFilter = (state) => state.dashboard.primaryFilter;
export default dashboardSlice.reducer;
