
import React, {useState, useEffect} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import {AGE_GROUP_MAP} from '../../utils/prepareDataSet';
import { useSelector, useDispatch } from 'react-redux';
import Summary from './Components/Summary';
import SecodaryFilters from './Components/SecondaryFilters';
import Charts from './Components/Charts';
import {
  useParams
} from "react-router-dom";

import {
  initialLoad,
  filterRecords,
  selectRecords,
  selectFilters
} from './dashboardSlice';
import Select from "react-select";


const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary
  },
  paperchart: {
    padding: theme.spacing(1),
    textAlign: 'right',
    color: theme.palette.text.secondary
  },
  checkbox:{
      display:'inline-block',
      padding:'10px'
  },
  barchartOptions:{
      textAlign:'right'
  },
  ul:{
      listStyle:'none',
      padding:'0px'
  }
}));
const DashboardContainer = (props) =>{
    let { id } = useParams();
    const classes = useStyles();
    const dispatch = useDispatch();
    const data = useSelector(selectRecords);
    const filters = useSelector(selectFilters);
    let ageGroups = [];
    // Special Handling for Age group because it requires range
    // TODO: Should be generic code to derive ranges for other numeric cols without using chart
    if(filters['ageGroupId'].options){
        ageGroups = filters['ageGroupId'].options 
            .map((group) => {
            const label = `${AGE_GROUP_MAP[group.value].start} to ${AGE_GROUP_MAP[group.value].end} ${group.label.slice(1)} `;
            const value = group.value;
            return  {value, label}
        });
    }

    const [selectDisease, setSelectDesease] = useState({
        multiValue: []
    });
    const [selectLocation, setSelectLocation] = useState({
        multiValue: [

        ]
    });
    const [selectAgeGroups, setAgeGroups] = useState({
        multiValue: [
        ]
    });

    const handleMultiChange = (option, name) =>{
        setSelectDesease({
            ...selectDisease,
            multiValue: option
        });
        dispatch(filterRecords({
            name,
            valArr: option
        }));
    }
    const handleSelectMultiChange = (name, option) =>{
        dispatch(filterRecords({
            name,
            valArr: option
        }));
    }
    // TODO: Should move to separate component 
    // However React allows flexibity to have function inside component to render JSX
    function FilterRow() {
        return (
        <React.Fragment>
            <Grid item xs={4}>
                <Paper className={classes.paper}>
                    Disease
                    <Select
                        name="diseaseFilters"
                        value={selectDisease.multiValue}
                        options={filters['disease'].options}
                        onChange={(option) => handleMultiChange(option, 'disease')}
                        isMulti
                    />
                </Paper>
            </Grid>
            <Grid item xs={4}>
                <Paper className={classes.paper}>
                    Age Group
                    <Select
                        name="ageGroupFilters"
                        value={selectAgeGroups.multiValue}
                        options={ageGroups}
                        onChange={(option) => {
                            setAgeGroups({
                                ...selectAgeGroups,
                                multiValue: option
                            });
                            handleSelectMultiChange('ageGroupId', option);
                        }}
                        
                        isMulti
                    />
                </Paper>
            </Grid>
            <Grid item xs={4}>
                <Paper className={classes.paper}>
                    Location -Zip Code
                    <Select
                        name="locationFilter"
                        value={selectLocation.multiValue}
                        options={filters['zipcode'].options}
                        onChange={(option) => {
                            setSelectLocation({
                                ...selectLocation,
                                multiValue: option
                            });
                            handleSelectMultiChange('zipcode', option);
                        }}
                        isMulti
                        />
                </Paper>
            </Grid>
        </React.Fragment>
        );
    }
    const handleCheckboxFilter = (name, filterName) =>{
        const isRemove = filters[filterName].selected.includes(name);
        let valArr = filters[filterName].selected.map((value)=>({value}));
        if(isRemove){
            valArr = valArr.filter((val) => val.value!==name);
            dispatch(filterRecords({
                name:filterName,
                valArr
            }));
            return;
        }
        valArr.push({
            value:name
        });
        dispatch(filterRecords({
            name:filterName,
            valArr
        }));
    }
    useEffect(()=>{
        //This is for demo purpose. Saved filters
        // Because react select has some issue Currently this is disabled
        let filters = {
            gender: {
                selected:['female', 'male', 'other']
            },// options and selected
            zipcode: {},
            employment_status: {},
            education: {},
            marital_status: {
                selected:['married', 'single', 'other']
            },
            ancestry: {},
            military_service: {
                selected:['yes', 'no']
            },
            disease: {},
            ageGroupId: {
            }
        };
        if(id==="3"){
            filters.military_service.selected = ['no'];
            filters.marital_status.selected = ['married', 'single'];
            filters.gender.selected = ['female'];
        }
        dispatch(initialLoad(filters)); // eslint-disable-next-line
    }, [id]);
    return(
        <div>
            <div className={classes.root}>
            <Grid container spacing={1}>
                <Grid container item xs={12} spacing={3}>
                <Summary 
                    data={data}
                    useStyles={useStyles}
                />
                <FilterRow />
                <SecodaryFilters
                    useStyles={useStyles}
                    filters={filters}
                    handleCheckboxFilter={handleCheckboxFilter}
                />
                <Charts 
                    data={data}
                    useStyles={useStyles}
                />
                </Grid>
            </Grid>
            </div>
        </div>
    )
}
export default DashboardContainer;