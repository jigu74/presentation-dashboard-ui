import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import React from 'react';

//TODO: Should be refactor to make it more genric checkbox based Filter rendering.
const SecodaryFilters =(props) =>{
    const {
        useStyles,
        handleCheckboxFilter,
        filters
    } = props;
    const classes = useStyles();
    return (
    <>
        <Grid item xs={4}>
            <Paper className={classes.paper}>
                Gender
                <div>
                    {['male', 'female', 'other'].map((name) =>{
                        return (<span className={classes.checkbox} key={name}>
                            <label>{name}:
                            
                            <input
                                id={`chk_${name}`}
                                type='checkbox'
                                checked={filters.gender.selected.includes(name)}
                                onChange={() => {
                                    handleCheckboxFilter(name, 'gender');
                                }}
                                />
                            </label>
                        </span>)
                    })}
                    
                </div>
            </Paper>
        </Grid>
        <Grid item xs={4}>
            <Paper className={classes.paper}>
                Martial status
                    <div>
                    {['married', 'single', 'other'].map((name) =>{
                        return (<span className={classes.checkbox} key={name}>
                            <label>{name}:
                            <input
                                id={`chk_${name}`}
                                type='checkbox'
                                checked={filters.marital_status.selected.includes(name)}
                                onChange={() => {
                                    handleCheckboxFilter(name, 'marital_status');
                                }}
                                />
                            </label>
                        </span>)
                    })}
                    
                </div>
            </Paper>
        </Grid>
        <Grid item xs={4}>
            <Paper className={classes.paper}>
                Military Service
                <div>
                {['yes', 'no'].map((name) =>{
                        return (<span className={classes.checkbox} key={name}>
                            <label>{name}:
                            <input
                                id={`chk_${name}`}
                                type='checkbox'
                                checked={filters.military_service.selected.includes(name)}
                                onChange={() => {
                                    handleCheckboxFilter(name, 'military_service');
                                }}
                                />
                            </label>
                        </span>)
                    })}
                    </div>
                
            </Paper>
        </Grid>
    </>
    );
}
export default SecodaryFilters;