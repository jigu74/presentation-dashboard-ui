
import Grid from '@material-ui/core/Grid';
import React, {useState} from 'react';
import Paper from '@material-ui/core/Paper';
import {groupBy} from '../../../utils/prepareDataSet';
import Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';
//Try to make it as generic as possible so user can slice and dice data/chart more meaningful way
const Charts =(props) =>{
    const {
        data,
        useStyles
    } = props;
    const classes = useStyles();
    const [barType, setBarType] = useState('daily_internet_use');
    const [barChartGrp, setBarChartGrp] = useState('1');
    const generateChartGroupingAvg = (firstGroupKey, secondGroupKey, avgNumColName ) => {

        const hMap = new Map();
        for(const record of data){
            let values = [];
            const key = record[firstGroupKey];
            if(hMap.has(key)){
                values = hMap.get(key);
            }
            const secondGrpVal = record[secondGroupKey];
            const avgNumColVal = record[avgNumColName];
            values.push({
                [secondGroupKey]:secondGrpVal,
                [avgNumColName]:avgNumColVal
            });
            hMap.set(key, values);
        }
        const internetCats = [...hMap.keys()];
        const avgVals = internetCats.map((cat) =>{
            const catSubMap = new Map();
            const catRecords = hMap.get(cat);
            for(const r of catRecords){
                let totalCount = 1;
                let totalUsage = r[avgNumColName];
                const innerKey = r[secondGroupKey];
                if(catSubMap.has(innerKey)){
                    totalCount += catSubMap.get(innerKey).totalCount;
                    totalUsage += catSubMap.get(innerKey).totalUsage;
                }
                catSubMap.set(innerKey, {
                    totalCount,
                    totalUsage
                    })
            };
            const catSummary = {};
            for(const [key, val] of catSubMap.entries()){
                catSummary[key] = val.totalUsage/val.totalCount
            }
            return {
                category:cat,
                summary: catSummary
            }
        });
        let secodGrp = groupBy(data, secondGroupKey) || [];
        const series = secodGrp.map((grp)=>{
            return {
                name:grp.key,
                data: avgVals.map((s) => s.summary[grp.key]|| 0 )
            }
        });
        const retObj = {internetCats,series};
        return retObj;

    };

    const generateBarChartOptions = (firstGroupKey, avgNumColName, chartData)=>{
        const chartOptions = {
                chart: {
                    type: 'bar'
                },
                title: {
                    text: `${avgNumColName} by ${firstGroupKey}`
                },
                xAxis: {
                    categories: chartData.internetCats,
                    title: {
                        text: null
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: avgNumColName,
                        align: 'high'
                    },
                    labels: {
                        overflow: 'justify'
                    }
                },
                 
                plotOptions: {
                    bar: {
                        dataLabels: {
                            enabled: true
                        }
                    }
                },
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'top',
                    x: -40,
                    y: 80,
                    floating: true,
                    borderWidth: 1,
                    backgroundColor:
                        Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
                    shadow: true
                },
                credits: {
                    enabled: false
                },
                series: chartData.series,
            }
    
        return chartOptions
    }

    let chidrenData = groupBy(data, 'children') || [];
    chidrenData = chidrenData.map((grp) =>{
        return{
            name:grp.key,
            y:grp.count
        }
    });
    const bartChartGroupMap ={
        "1": ['education', 'employment_status'],
        "2": ['disease', 'ancestry'],
        "3": ['ancestry', 'disease']
    };
    const [firstGrp, secondGrp] = bartChartGroupMap[barChartGrp];
 
    const barChartData = generateChartGroupingAvg(firstGrp, secondGrp, barType);
    let chartOptions = generateBarChartOptions(firstGrp, barType, barChartData);
    const childOptions = {
            chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'By Chidren'
        },
        series: [{data:chidrenData}]
    }
    return (
    <React.Fragment>
        
        <Grid item xs={4}>
            <HighchartsReact
                highcharts={Highcharts}
                options={childOptions}
            />
        </Grid>
        <Grid item xs={8}>
            <Paper className={classes.paperchart}>
            <label>
                Group By
               <select value={barChartGrp} onChange={(e) => setBarChartGrp(e.target.value)}>
                    <option value="1">Education -- Employment Status</option>
                    <option value="2">disease -ancestry </option>
                    <option value="3">ancestry - disease</option>
                </select>

            </label>
            <label>               
            <input
                id='chk_daily'
                type='radio'
                name='chartSel'
                checked={barType === 'daily_internet_use'}
                onChange={() => {
                    const type = barType === 'daily_internet_use'?
                        'avg_commute': 'daily_internet_use';
                    setBarType(type)
                }}
                />
                Daily Internet usage
            </label>
            <label>            
            <input
                id='chk_avg'
                name='chartSel'
                type='radio'
                checked={barType !== 'daily_internet_use'}
                onChange={() => {
                    const type = barType === 'daily_internet_use'?
                        'avg_commute': 'daily_internet_use';
                    setBarType(type)
                }}
                />
                Avg Daily Mpg:
            </label>
            </Paper>
            <HighchartsReact
                highcharts={Highcharts}
                options={chartOptions}
            />
        </Grid>
    </React.Fragment>
    );
    
}
export default Charts;