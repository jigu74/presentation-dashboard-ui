import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import React from 'react';

//This is to demo purpose to show Names of Users if count is less than 20
const Summary =(props) =>{
    const {
        data,
        useStyles
    } = props;
    const classes = useStyles();
    return (
        <Grid item xs={12}>
            <Paper className={classes.paper}>
                No of  patients : 
                {data && data.length}
                {data && data.length && data.length < 20 &&
                <ul className={classes.ul}>
                    {data.map((record) =>{
                        return(
                            <li key={record.id}>
                                {record.id}
                            </li>
                        )
                    })}
                </ul>
                }
            </Paper>
        </Grid>
    );
}
export default Summary;