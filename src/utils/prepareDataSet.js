
export const AGE_GROUP_MAP ={
    '1':{
        start:0,
        end: 18
    },
    '2':{
        start:18,
        end: 34
    },
    '3':{
        start:34,
        end: 50
    },
    '4':{
        start:50,
        end: 65
    },
    '5':{
        start:65,
        end: 80
    },
    '6':{
        start:80,
        end: Infinity
    }
}

const populateAgeGroups = (records) =>{
    return records.map((record) =>{
        
        const {dob} = record;
        const dtDob = new Date(dob);
        let ageInMs = Date.now() - dtDob.getTime();
        const age = Number((ageInMs/(365*24*60*60*1000)).toFixed(2));
        let ageGroupId;
         
        for(const groupId in AGE_GROUP_MAP){
            const ageGroup = AGE_GROUP_MAP[groupId];
            const {
                start,
                end
            } = ageGroup;
            if( age >= start && age < end){
                ageGroupId = groupId;
                break;
            }
        }
        return {
            ...record,
            zipcode: record.zipcode.toString(),
            age,
            ageGroupId
        }
    });
}
// Generic Hashmap for React Select with Summary(Count and %)
const groupBy = (records, key) => {
    const group= records.reduce((acc, curr)=>{
        acc[curr[key]] = acc[curr[key]] ? acc[curr[key]] + 1:1
        return acc;
    } ,{});
    const total = records.length;
    return Object.keys(group).map((k) =>{
        const percentage = ((group[k]*100)/total).toFixed(2)
        return ({
            key:k,
            labelWithSummary:`${k} #${group[k]} ${percentage}%`,
            count:group[k]
        })
    })
}

export { populateAgeGroups, groupBy}