import { configureStore } from '@reduxjs/toolkit';
import dashboardReducer from '../Containers/Dashboards/dashboardSlice';

export const store = configureStore({
  reducer: {
    dashboard: dashboardReducer
  },
});
